﻿using BookManagement.Domain.Interfaces;
using BookManagement.Infrastructure.Data;
using BookManagement.Infrastructure.Data.Repositories;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BookManagement.Infrastructure.IoC
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //BookManagement.Application
            //services.AddScoped<IBookService, IBookService>();

            //BookManagement.Infrastructure.Data
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IBookRepository, BookRepository>();

            //MeditR
            var assembly = typeof(Application.Features.Books.Queries.GetAllBooksQuery).Assembly;
            services.AddMediatR(assembly);
            
            //AddAutoMapper
            services.AddAutoMapper(assembly);
        }
    }
}
