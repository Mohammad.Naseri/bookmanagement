﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManagement.Domain.Entities
{
   public class BookBorrower:BaseEntity
    {
        public virtual Book Book { get; set; }
        public int BookId { get; set; }
        public virtual Borrower Borrower { get; set; }
        public int BorrowerId { get; set; }
        public DateTime DateofBorrowing { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}
