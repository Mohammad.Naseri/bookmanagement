﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManagement.Domain.Entities
{
    public class Borrower : Person
    {
        public int NationalCode { get; set; }
        public string HomePhoneNumber { get; set; }
    }
}
