﻿using BookManagement.Domain.Interfaces;
using BookManagement.Domain.Entities;
using BookManagement.Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManagement.Infrastructure.Data.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(BookManagementDbContext bookManagementDbContext) : base(bookManagementDbContext)
        {

        }
    }
}
