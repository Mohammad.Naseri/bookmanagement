﻿using BookManagement.Infrastructure.Data.Context;
using BookManagement.Infrastructure.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookManagement.Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BookManagementDbContext context;
        private bool disposed;
        private Dictionary<string, object> repositories;

        public UnitOfWork(BookManagementDbContext context)
        {
            this.context = context;
        }
        public int SaveChanges()
        {
            return context.SaveChanges();
        }
        public async virtual Task<int> SaveChangesAsync()
        {
            return await context.SaveChangesAsync();
        }
        public Repository<T> GetRepository<T>() where T : class
        {
            if (repositories == null)
            {
                repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;

            if (!repositories.ContainsKey(type))
            {
                var repositoryType = typeof(Repository<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), context);
                repositories.Add(type, repositoryInstance);
            }
            return (Repository<T>)repositories[type];
        }
        public void BeginTransaction()
        {
            context.Database.BeginTransaction();
        }

        public void Commit()
        {
            context.Database.CommitTransaction();
        }

        public void Rollback()
        {
            context.Database.RollbackTransaction();
        }

        public async Task CommitAsync()
        {
             await context.Database.CommitTransactionAsync();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }
    }
}
