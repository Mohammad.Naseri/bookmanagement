﻿using AutoMapper;
using static BookManagement.Application.Features.Books.Commands.CreateBookCommand;
using static BookManagement.Application.Features.Books.Commands.DeleteBookCommand;
using static BookManagement.Application.Features.Books.Commands.UpdateBookCommand;
using BookManagement.Application.Features.Books.Commands;
using BookManagement.Domain.Interfaces;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BookManagement.Application.UnitTests.Features.Books.Commands
{
    public class BookCommandsTester
    {
        private readonly IMapper _mapper;
        private readonly Mock<IBookRepository> _mockBookRepository;
        public BookCommandsTester()
        {
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<Mapping.Mapper>()).CreateMapper();
            _mockBookRepository = Mocks.RepositoryMocks.GetBookRepository();
        }
        [Fact]
        public async Task CreateBookCommand_ShouldCreateBook()
        {
            var book = new CreateBookCommand()
            {
                CategoryId = 3,//Medicl
                Title = "When Breath Becomes Air",
                CopyNum = 22,
                ISBN = "978-0812988406",
                Hardcover = 228,
                Copyright = 2016,
                Publisher = "Random House; 1st edition (January 12, 2016)",
                Language = "English",
                ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/41ulxo+W2pL._SX326_BO1,204,203,200_.jpg",
                Summery = "When Breath Becomes Air is a powerful look at a stage IV lung cancer diagnosis through the eyes of a neurosurgeon."
            };
            var createBookCommandHandler = new CreateBookCommandHandler(_mockBookRepository.Object, _mapper);
            var result = await createBookCommandHandler.Handle(book, CancellationToken.None);

            var allBooks = await _mockBookRepository.Object.GetAllAsyn();

            allBooks.Count().ShouldBe(6);
        }

        [Fact]
        public async Task UpdateBookCommand_ShouldUpdateABookWithEditedValues()
        {
            int bookId = 1;
            var book = new UpdateBookCommand()
            {
                Id = bookId,
                CategoryId = 2,//Romance
                Title = "The Return",
                CopyNum = 14,
                ISBN = "978-1538728574",
                Hardcover = 368,
                Copyright = 2020,
                Publisher = "Grand Central Publishing (September 29, 2020)",
                Language = "English",
                ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/51Y2VGacBDL._SX327_BO1,204,203,200_.jpg",
                Summery = "As much a family drama as it is a love story... If you, like Trevor, are looking to slow down and focus on what's really important, The Return is the heartwarming read you've been waiting for."
            };
            var updateBookCommandHandler = new UpdateBookCommandHandler(_mockBookRepository.Object, _mapper);
            var result = await updateBookCommandHandler.Handle(book, CancellationToken.None);

            var editedBook = await _mockBookRepository.Object.GetByIdAsync(bookId);

            editedBook.Title.ShouldBe("The Return");
            editedBook.ISBN.ShouldBe("978-1538728574");
        }

        //[Fact]
        //public async Task DeleteBookCommand_ShouldDeleteABookWithSpecificId()
        //{
        //    var deleteBookCommandHandler = new DeleteBookCommandHandler(_mockBookRepository.Object);
        //    await deleteBookCommandHandler.Handle(new DeleteBookCommand { Id = 1 }, CancellationToken.None);
        //    var allBooks = await _mockBookRepository.Object.GetAllAsyn();

        //    allBooks.Count().ShouldBe(4);
        //}
    }
}
