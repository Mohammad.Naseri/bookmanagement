﻿using AutoMapper;
using BookManagement.Application.Features.Books;
using BookManagement.Application.Features.Books.Commands;
using BookManagement.Application.Features.Books.Queries;
using BookManagement.Domain.Interfaces;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using static BookManagement.Application.Features.Books.Commands.CreateBookCommand;
using static BookManagement.Application.Features.Books.Queries.GetAllBooksQuery;
using static BookManagement.Application.Features.Books.Queries.GetBookByIdQuery;

namespace BookManagement.Application.UnitTests.Features.Books.Queries
{
    public class BookQueriesTester
    {
        private readonly IMapper _mapper;
        private readonly Mock<IBookRepository> _mockBookRepository;
        public BookQueriesTester()
        {
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<Mapping.Mapper>()).CreateMapper();
            _mockBookRepository = Mocks.RepositoryMocks.GetBookRepository();
        }

        [Fact]
        public async Task GetBookByIdQuery_ShouldReturnSpecificBook()
        {
            var query = new GetBookByIdQuery() { Id = 4 };
            var getBookByIdQueryHandler = new GetBookByIdQueryHandler(_mockBookRepository.Object, _mapper);
            var result = await getBookByIdQueryHandler.Handle(query, CancellationToken.None);
            result.ShouldNotBeNull<GetBookByIdResponseModel>();
            result.Title.ShouldBe("Head First Design Patterns: A Brain-Friendly Guide");
        }
        [Theory]
        [InlineData(1, "Clean Code: A Handbook of Agile Software Craftsmanship")]
        [InlineData(2, "Introduction to Algorithms")]
        [InlineData(3, "Structure and Interpretation of Computer Programs (SICP)")]
        [InlineData(4, "Head First Design Patterns: A Brain-Friendly Guide")]
        [InlineData(5, "Refactoring: Improving the Design of Existing Code")]
        public async Task GetBookByIdQuery_ShouldReturnSpecificBookForEachOne(int id, string expectedBookTitle)
        {
            var query = new GetBookByIdQuery() { Id = id };
            var getBookByIdQueryHandler = new GetBookByIdQueryHandler(_mockBookRepository.Object, _mapper);
            var result = await getBookByIdQueryHandler.Handle(query, CancellationToken.None);
            result.ShouldNotBeNull<GetBookByIdResponseModel>();
            result.Title.ShouldBe(expectedBookTitle);
        }
        [Fact]
        public async Task GetAllBooksQuery_ShouldReturnAllBooks()
        {
            var getAllBooksQueryHandler = new GetAllBooksQueryHandler(_mockBookRepository.Object, _mapper);
            var results = await getAllBooksQueryHandler.Handle(new GetAllBooksQuery(), CancellationToken.None);
            results.ShouldBeOfType<List<GetAllBooksResponseModel>>();
            results.Count.ShouldBe(5);
        }
    }
}
