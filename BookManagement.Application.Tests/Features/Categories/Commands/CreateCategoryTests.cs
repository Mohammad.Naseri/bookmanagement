﻿using AutoMapper;
using BookManagement.Application.Features.Categories.Commands.CreateCategory;
using BookManagement.Domain.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BookManagement.Application.UnitTests.Features.Categories.Commands
{
    public class CreateCategoryTests
    {
        private readonly IMapper _mapper;
        private readonly Mock<ICategoryRepository> _mockCategoryRepository;
        public CreateCategoryTests()
        {
            _mockCategoryRepository = Mocks.RepositoryMocks.GetCategoryRepository();
            _mapper = new MapperConfiguration(cfg => { cfg.AddProfile<Mapping.Mapper>(); }).CreateMapper();
        }
        [Fact]
        public async Task Handle_ShoudAddValidCategoryToRepository()
        {
            var createCategoryCommand = new CreateCategoryCommand() { Name = "Social Sciences" };
            var createCategoryCommandHandler = new CreateCategoryCommandHandler(_mockCategoryRepository.Object, _mapper);
            await createCategoryCommandHandler.Handle(createCategoryCommand, CancellationToken.None);
            var allCategories = await _mockCategoryRepository.Object.GetAllAsyn();
            Assert.Equal(7, allCategories.Count());
        }
    }
}
