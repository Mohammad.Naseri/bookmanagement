﻿using AutoMapper;
using BookManagement.Application.Features.Categories.Queries.GetCategoriesList;
using BookManagement.Application.Features.Categories.Queries.GetCategoryById;
using BookManagement.Domain.Interfaces;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BookManagement.Application.UnitTests.Features.Categories.Queries
{
    public class CaregoryQueriesTester
    {
        private readonly Mock<ICategoryRepository> _mockCategoryRepository;
        private readonly IMapper _mapper;

        public CaregoryQueriesTester()
        {
            _mockCategoryRepository = Mocks.RepositoryMocks.GetCategoryRepository();
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<Mapping.Mapper>()).CreateMapper();
        }

        [Fact]
        public async Task GetCategoriesList_ShouldReturnAllItems()
        {
            var query = new GetCategoriesListQuery();
            var getCategoriesListQueryHandler = new GetCategoriesListQueryHandler(_mockCategoryRepository.Object, _mapper);
            var categories = await getCategoriesListQueryHandler.Handle(query, CancellationToken.None);
            categories.ShouldBeOfType<List<CategoryListVm>>();
            categories.Count.ShouldBe(6);
        }

        [Fact]
        public async Task GetCategoryById_ShouldReturnSpecificItem()
        {
            int id = 2;
            string expected = "Romance";

            var query = new GetCategoryByIdQuery { Id = id };
            var getCategoryByIdQueryHandler = new GetCategoryByIdQueryHandler(_mockCategoryRepository.Object, _mapper);
            var actual = await getCategoryByIdQueryHandler.Handle(query, CancellationToken.None);
            actual.ShouldNotBeNull<GetCategoryByIdVm>();
            actual.Name.ShouldBe(expected);
        }
        [Theory]
        [InlineData(1,"Medicl")]
        [InlineData(2, "Romance")]
        [InlineData(3, "Science & Math")]
        [InlineData(4, "Entertainment")]
        [InlineData(5, "Religion")]
        [InlineData(6, "History")]
        public async Task GetCategoryById_ShouldMatchToSpecificItems(int id,string expectedName)
        {
            var query = new GetCategoryByIdQuery { Id = id };
            var getCategoryByIdQueryHandler = new GetCategoryByIdQueryHandler(_mockCategoryRepository.Object, _mapper);
            var actual = await getCategoryByIdQueryHandler.Handle(query, CancellationToken.None);
            actual.ShouldNotBeNull<GetCategoryByIdVm>();
            actual.Name.ShouldBe(expectedName);
        }
    }
}
