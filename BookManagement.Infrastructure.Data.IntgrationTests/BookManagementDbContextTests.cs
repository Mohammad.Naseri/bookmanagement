using BookManagement.Domain.Entities;
using BookManagement.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace BookManagement.Infrastructure.Data.IntgrationTests
{
    public class BookManagementDbContextTests
    {
        private readonly BookManagementDbContext _bookManagementDbContext;
        public BookManagementDbContextTests()
        {
            var options = new DbContextOptionsBuilder<BookManagementDbContext>()
                            .UseInMemoryDatabase("BookManagementInMemoryDatabase").Options;

            _bookManagementDbContext = new BookManagementDbContext(options);
        }

        [Fact]
        public async Task AddNewCategoryShouldIncreaseCount()
        {
            var categoriesBeforeAdd = await _bookManagementDbContext.Categories.ToListAsync();

            var category = new Category { Id = 8, Name = "Sports" };
            _bookManagementDbContext.Categories.Add(category);
            await _bookManagementDbContext.SaveChangesAsync();

            var categoriesAfterAdd = await _bookManagementDbContext.Categories.ToListAsync();

            Assert.NotEqual(categoriesBeforeAdd.Count, categoriesAfterAdd.Count);
            categoriesBeforeAdd.Count.ShouldBe(0);
            categoriesAfterAdd.Count.ShouldBe(1);
        }
    }
}
