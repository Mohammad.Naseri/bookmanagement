﻿using AutoMapper;
using BookManagement.Application.Features.Books;
using BookManagement.Application.Features.Books.Commands;
using BookManagement.Application.Features.Categories.Commands.CreateCategory;
using BookManagement.Application.Features.Categories.Commands.UpdateCategory;
using BookManagement.Application.Features.Categories.Queries.GetCategoriesList;
using BookManagement.Application.Features.Categories.Queries.GetCategoryById;
using BookManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManagement.Application.Mapping
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<Book, GetAllBooksResponseModel>().ReverseMap();
            CreateMap<Book, GetBookByIdResponseModel>().ReverseMap();
            CreateMap<CreateBookCommand, Book>().ReverseMap();
            CreateMap<UpdateBookCommand, Book>().ReverseMap();
            CreateMap<GetBookByIdResponseModel, UpdateBookCommand>()
                .ForMember(prop => prop.Id, opt => opt.Ignore()).ReverseMap();
 
            //CreateMap<Category, GetAllCategoriesResponseModel>().ReverseMap();

            CreateMap<Category, CategoryListVm>().ReverseMap();
            CreateMap<Category, CreateCategoryDto>().ReverseMap();
            
            CreateMap<Category, GetCategoryByIdVm>().ReverseMap();
            CreateMap<GetCategoryByIdVm,UpdateCategoryCommand >().ReverseMap();
            
        }
    }
}
