using AutoMapper;
using BookManagement.Application.Features.Books;
using BookManagement.Application.Features.Books.Queries;
using BookManagement.Mvc.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace BookManagement.IntegrationTests
{
    public class BookControllerTests
    {
        private Mock<IMediator> _mediator;
        private readonly IMapper _mapper;
        public BookControllerTests()
        {
            _mediator = new Mock<IMediator>();
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<Application.Mapping.Mapper>()).CreateMapper();
        }

        [Fact]
        public void Details_ShouldReturn_GetBookByIdResponseModel()
        {
            _mediator.Setup(x => x.Send(It.IsAny<GetBookByIdQuery>(), new CancellationToken())).
                ReturnsAsync(new GetBookByIdResponseModel());

            var bookController = new BookController(_mediator.Object,_mapper);

            //Action
            var result = bookController.Details(2);

            //Assert
            Assert.IsType<Task<IActionResult>>(result);
            var responseModel = (GetBookByIdResponseModel)((ViewResult)result.Result).Model;
            Assert.IsType<GetBookByIdResponseModel>(responseModel);
        }
    }
}
